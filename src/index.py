import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib
import plotly.graph_objects as go
#import numpy_financial as npf

#INVERSION INCIAL
invIni = np.around(np.random.triangular(-130000, -100000, -80000, 1), 0) # Meter datos Estimacion pesimista - probable - optimista
invIni = invIni.astype(int)

#INDICES
indices = np.around(np.random.uniform(0,4,5), 0) # Meter DEL - AL, y cuantos datos se desea generar
indices = indices.astype(int) #Convierte los valores del arrreglo a enteros

#FLUJO
flujo = [20000, 30000, 40000, 50000, 60000] #Meter los valores que tendra nuestro flujo

#NUEVO FLUJO SEGUN INDICES (ARREGLO NUEVO)
flujoNuevo = np.take(flujo,indices)

#VALOR DE RESCATE
rescate = np.around(np.random.triangular(16000, 20000, 26000, 1), 0) #Meter datos Estimacion pesimista - probable - optimista
rescate = rescate.astype(int)

#INFLACION
inflacion = 1 - np.around(np.random.triangular(1-0.25, 1-0.20, 1-0.15, 1), 3) #Meter datos Estimacion pesimista - probable - optimista
#inflacion = inflacion.astype(int)
descuento = np.around(0.25+inflacion+(0.25*inflacion), 3)

#SUMAR EL ULTIMO VALOR DEL FLUJO CON EL VALOR DE RESCATE Y CONCATENAR EL FLUJO RESULTANTE CON INV-INI
suma = np.take(flujoNuevo,4) + rescate
flujoNuevo[4] = suma

#CONCATENACION DE LA INVERSION INCIAL CON EL FLUJO NUEVO
arregloFinal = np.concatenate((invIni,flujoNuevo),axis=0)


#CALCULAR EL VALOR ACTUAL invIni/(1+Tinteres)^n
#Vactual = -invIni/(1+0.10)**5

#CALCULAR EL VALOR FUTURO invIni*(1+Tinteres)^n
#Vfuturo = -invIni*(1+0.10)**5

#CALCULAR EL RATE [np.rate(periodos,pago,Vpresente,Vfuturo,)]
#rate = np.rate(5,-invIni,Vactual,Vfuturo)
#rate = 1/(1+0.10)**5

#REALIZAR EL CALUCULO VPN (NPV EN INGLES) [np.npv(rate,cashflow)]
vpn =  np.npv(descuento,arregloFinal)


# Este metodo regresa un numpy array con [inv_ini , 1, 2, 3, 4, 5, VPN, inflacion] (1,2,3,4,5 --> es el flujo neto)
def arreglo():
  invIni = np.around(np.random.triangular(-130000, -100000, -80000, 1), 0) # Meter datos Estimacion pesimista - probable - optimista
  invIni = invIni.astype(int)

  #INDICES
  indices = np.around(np.random.uniform(0,4,5), 0) # Meter DEL - AL, y cuantos datos se desea generar
  indices = indices.astype(int) #Convierte los valores del arrreglo a enteros

  #FLUJO
  flujo = [20000, 30000, 40000, 50000, 60000] #Meter los valores que tendra nuestro flujo

  #NUEVO FLUJO SEGUN INDICES (ARREGLO NUEVO)
  flujoNuevo = np.take(flujo,indices)

  #VALOR DE RESCATE
  rescate = np.around(np.random.triangular(16000, 20000, 26000, 1), 0) #Meter datos Estimacion pesimista - probable - optimista
  rescate = rescate.astype(int)

  #INFLACION

  # LA inflacion es alta y por eso no es rentable invertir modificando la inflación da buenos resultados
  inflacion = 1 - np.around(np.random.triangular(1-0.03, 1-0.015, 1-0.00, 1), 3) #Meter datos Estimacion pesimista - probable - optimista
  #inflacion = 1 - np.around(np.random.triangular(1-0.25, 1-0.20, 1-0.15, 1), 3) #Meter datos Estimacion pesimista - probable - optimista
  #inflacion = inflacion.astype(int)
  descuento = np.around(0.15+inflacion+(0.15*inflacion), 8)

  #SUMAR EL ULTIMO VALOR DEL FLUJO CON EL VALOR DE RESCATE Y CONCATENAR EL FLUJO RESULTANTE CON INV-INI
  suma = np.take(flujoNuevo,4) + rescate
  flujoNuevo[4] = suma

  #CONCATENACION DE LA INVERSION INCIAL CON EL FLUJO NUEVO
  arregloFinal = np.concatenate((invIni,flujoNuevo),axis=0)


  #CALCULAR EL VALOR ACTUAL invIni/(1+Tinteres)^n
  #Vactual = -invIni/(1+0.10)**5

  #CALCULAR EL VALOR FUTURO invIni*(1+Tinteres)^n
  #Vfuturo = -invIni*(1+0.10)**5

  #CALCULAR EL RATE [np.rate(periodos,pago,Vpresente,Vfuturo,)]
  #rate = np.rate(5,-invIni,Vactual,Vfuturo)
  #rate = 1/(1+0.10)**5

  #REALIZAR EL CALUCULO VPN (NPV EN INGLES) [np.npv(rate,cashflow)]
  vpn =  np.npv(descuento,arregloFinal)

  res = np.concatenate((arregloFinal, vpn, inflacion), axis = None)
  return res

#TERMINA EL METODO arreglo()

#Corridas
# corridas construye un dataframe de tamaño n difinido por --> range(n)
corridas = pd.DataFrame(columns=  ["inv_ini", 'año_1', 'año_2', 'año_3', 'año_4', 'año_5', 'VPN', 'inflacion'], index = range(100))
for i in corridas.index :
      corridas.iloc[i] = arreglo()
    
print(corridas)

# HISTOGRAMA VPN
corridas.sort_values(by=['VPN'], inplace=True) # ordenar en dataframe de menor a mayor segun el VPN
vpn_data = corridas['VPN']
tabla2 = corridas.to_numpy()
array = vpn_data.to_numpy()


print(array)
#
fragmentos = np.around(np.linspace(array[0], array[-1], 21), 3 )
bins = fragmentos.tolist()
print(bins)
plt.hist(array, bins = bins, orientation='vertical')

plt.title('Histograma VPN')
plt.xlabel('valores del VPN')
plt.ylabel('Total repeticiones')
plt.show()


#tir = np.irr([-117421, 20000, 30000,  40000, 50000, 70000]);

print("Inversion Incial: ", invIni)
print("Indices generados: ",indices)
print("Flujo: ",flujo)
print("Flujo nuevo: ",flujoNuevo)
print("Valor de rescate: ",rescate)
print("Inflacion: ",inflacion)
print("Tasa de descuento nominal: ", descuento)
#print("Suma: ",suma)
print("Arreglo final concatenado: ",arregloFinal)
#print("Valor actual: ", Vactual)
#print("Valor futuro: ", Vfuturo)
#print("Rate: ", rate)
print("VPN: ",vpn)
