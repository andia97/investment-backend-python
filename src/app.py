from flask import Flask, request,  jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS, cross_origin
import numpy as np
import pandas as pd
import json
import matplotlib
import plotly.graph_objects as go
from scipy.stats import norm
import matplotlib.pyplot as plt
# Data  base  config
app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://root@localhost/investemen-db-py'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False

db = SQLAlchemy(app)
ma = Marshmallow(app)

class Proyect(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_user = db.Column(db.Integer)
    name = db.Column(db.String(200))
    def __init__(self, name, id_user):
        self.name = name
        self.id_user = id_user
db.create_all()
class ProyectSchema(ma.Schema):
    class Meta:
        fields =  ('id', 'id_user', 'name')

proyc_schema = ProyectSchema()
proycs_schema = ProyectSchema(many=True)

class Data_Uno(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_proyc = db.Column(db.Integer, unique=True)
    media_first = db.Column(db.Integer)
    var_first = db.Column(db.Integer)
    media_second = db.Column(db.Integer)
    var_second = db.Column(db.Integer)
    year = db.Column(db.Integer)

    def __init__(self, id_proyc, media_first, var_first, media_second, var_second, year):
        self.id_proyc = id_proyc
        self.media_first = media_first
        self.var_first = var_first
        self.media_second = media_second
        self.var_second = var_second
        self.year = year
db.create_all()
class DataUnoSchema(ma.Schema):
    class Meta:
        fields =  ('id', 'id_proyc', 'media_first', 'var_first','media_second', 'var_second', 'year')

data_uno_schema = DataUnoSchema()
datas_uno_schema = DataUnoSchema(many=True)
class Data_Dos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_proyc = db.Column(db.Integer)
    tipo = db.Column(db.String(200))
    est_pes = db.Column(db.String(200))
    est_prob = db.Column(db.String(200))
    est_opt = db.Column(db.String(200))
    year = db.Column(db.String(200))

    def __init__(self, id_proyc, tipo, est_pes, est_prob, est_opt, year):
        self.id_proyc = id_proyc
        self.tipo = tipo
        self.est_pes = est_pes
        self.est_prob = est_prob
        self.est_opt = est_opt
        self.year = year
db.create_all()
class DataDosSchema(ma.Schema):
    class Meta:
        fields =  ('id', 'id_proyc', 'tipo', 'est_pes','est_prob', 'est_opt', 'year')

data_dos_schema = DataDosSchema()
datas_dos_schema = DataDosSchema(many=True)
class Flujo_Neto(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_proyc = db.Column(db.Integer)
    anio = db.Column(db.String(200))
    flujo_neto = db.Column(db.Integer)
    def __init__(self, id_proyc, anio, flujo_neto):
        self.id_proyc = id_proyc
        self.anio = anio
        self.flujo_neto = flujo_neto
db.create_all()
class FlujoNetoSchema(ma.Schema):
    class Meta:
        fields =  ('id', 'id_proyc', 'anio', 'flujo_neto')

flujo_neto_schema = FlujoNetoSchema()
flujos_netos_schema = FlujoNetoSchema(many=True)

class Result_Uno(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_data_proyc = db.Column(db.Integer, unique=True)
    investment = db.Column(db.String(100))
    tir = db.Column(db.String(100))
    def __init__(self, titid_data_proycle, investment, tir):
        self.id_data_proyc = id_data_proyc
        self.investment = investment
        self.tir = tir
db.create_all()
class ResultUnoSchema(ma.Schema):
    class Meta:
        fields =  ('id', 'title', 'description')
result_uno_schema = ResultUnoSchema()
results_uno_schema = ResultUnoSchema(many=True)


# endPoints
@app.route('/proyect', methods=['POST'])
@cross_origin()
def create_data_proyect():
    newName = request.json['name']
    newIdUser = request.json['id_user']
    newInvestment = Proyect(newName, newIdUser)
    db.session.add(newInvestment)
    db.session.commit()
    return proyc_schema.jsonify(newInvestment)
@app.route('/dataProyect', methods=['POST'])
@cross_origin()
def create_proyect():
    newIdProyc = request.json['id_proyc']
    newVarFirst = request.json['var_first']
    newMediaFirst = request.json['media_first']
    newVarSecond = request.json['var_second']
    newMediaSecond = request.json['media_second']
    newYear = request.json['year']
    newDataProyect = Data_Uno(newIdProyc, newMediaFirst, newVarFirst, newMediaSecond, newVarSecond, newYear)
    db.session.add(newDataProyect)
    db.session.commit()
    return data_uno_schema.jsonify(newDataProyect)
@app.route('/secondDataProyect', methods=['POST'])
@cross_origin()
def create_second_proyect():
    newIdProyc = request.json['id_proyc']
    newtipo = request.json['tipo']
    newEstPes = request.json['est_pes']
    newEstProb = request.json['est_prob']
    newEstOpt = request.json['est_opt']
    newYear = request.json['year']
    newSecondDataProyect = Data_Dos(newIdProyc, newtipo, newEstPes, newEstProb, newEstOpt, newYear)
    db.session.add(newSecondDataProyect)
    db.session.commit()
    return data_dos_schema.jsonify(newSecondDataProyect)

@app.route('/flujoSecondDataProyect', methods=['POST'])
@cross_origin()
def create_flujo_second_proyect():
    newIdProyc = request.json['id_proyc']
    values = request.json['values']
    for value in values:
        newAnio  = value['anio']
        newFlujo  = value['flujo_neto']
        newFlujoSecondDataProyect = Flujo_Neto(newIdProyc, newAnio,newFlujo )
        db.session.add(newFlujoSecondDataProyect)
        db.session.commit()
    return jsonify("Added  succsesfully")

# ################################################## #
# end point que se encarga de realizar la operación de la inversion numero 1

@app.route('/result-data-first/<id>', methods=['GET'])
def getFirstResult(id):
    investments = db.session.query(Data_Uno).filter(
        Data_Uno.id == id
    )
    for investment in investments:
        idData = investment.id
        invest_media = investment.media_first     
        invest_var = investment.var_first     
        flujo_media = investment.media_second     
        flujo_var = investment.var_second
        year = investment.year
        print("year:" + str(year))
    # calculate(invest_media, invest_var, flujo_media, flujo_var, year)
    # return datas_uno_schema.jsonify(investments)
    return calculate(invest_media, invest_var, flujo_media, flujo_var, year)
    # result = data_uno_schema.dump(allTask)
    # return jsonify({"idData":idData,"varianza":invest_var,"media":invest_media})
def calculate(ivMedia, ivVar, fjMedia, fjVar, year):
    #inversionInicial
    invertion = round(np.random.normal(ivMedia,ivVar),3)
    print("Inversion: "+ str(invertion))
    auxTir = []
    auxTir.append(-invertion)

    # Flujo neto en el perido t (años)
    fjNeto = np.random.normal(fjMedia, fjVar, year)
    print("Flujo Neto: " + str(fjNeto))
    auxTir.extend(fjNeto)
    flujoNeto = []
    flujoNeto.extend(fjNeto)
    #Calculo del TIR
    tir = round(np.irr(auxTir),5)
    return jsonify({"invertion":invertion,"tir":tir,"fjNeto":flujoNeto})

@app.route('/result-data-second/<idProyc>', methods=['GET'])
def getSecondResult(idProyc):
    result_2 = db.session.query(Data_Dos).filter(
        Data_Dos.id_proyc == idProyc
    )
    datos_flujo = db.session.query(Flujo_Neto).filter(
        Flujo_Neto.id_proyc == idProyc
    )
    newArrayFlujo = []
    newArrayColumn = []
    for flujo in datos_flujo:
        anio = flujo.anio
        flujo_neto = flujo.flujo_neto
        newArrayFlujo.append(flujo_neto)
        newArrayColumn.append(anio)
    # print(newArrayFlujo)
    for data in result_2:
        tipo = data.tipo;
        # print(tipo)
        if tipo == 'invest':
            invest_est_pes = data.est_pes
            invest_est_pro =  data.est_prob
            invest_est_opt =  data.est_opt
            # print("Este es el tipo: " + tipo + "***** Pesimista: " + str(invest_est_pes) + "***** Probable: " + str(invest_est_pes) + "***** Optimista: " + str(invest_est_opt))
        if tipo == 'rescate':
            rescate_est_pes = data.est_pes
            rescate_est_pro =  data.est_prob
            rescate_est_opt =  data.est_opt
            # print("Este es el tipo: " + tipo + "***** Pesimista: " + str(rescate_est_pes) + "***** Probable: " + str(rescate_est_pes) + "***** Optimista: " + str(rescate_est_opt))
        if tipo == 'tasa':
            tasa_est_pes = data.est_pes
            tasa_est_pro =  data.est_prob
            tasa_est_opt =  data.est_opt
            # print("Este es el tipo: " + tipo + "***** Pesimista: " + str(tasa_est_pes) + "***** Probable: " + str(tasa_est_pes) + "***** Optimista: " + str(tasa_est_opt))
    # calculate(invest_media, invest_var, flujo_media, flujo_var, year)
    # return datas_uno_schema.jsonify(investments)
    arrayColumns = []
    for data in newArrayColumn:
        arrayColumns.append("anio_" + str(data))
    print(arrayColumns)
    arrayColumns.append('inv_ini')
    arrayColumns.append('VPN')
    arrayColumns.append('inflacion')
    corridas = pd.DataFrame(columns=  arrayColumns, index = range(100))
    for i in corridas.index :
        corridas.iloc[i] = calculatesecondEscenary(invest_est_pes, invest_est_pro, invest_est_opt, rescate_est_pes, rescate_est_pro, rescate_est_opt, tasa_est_pes, tasa_est_pro, tasa_est_opt, newArrayFlujo)
    result = corridas.to_json(orient="table")
    parsed = json.loads(result)
    # json.dumps(parsed, indent=4)
    # print(corridas)

    # print(bins)
    return json.dumps(parsed, indent=4)

    
def calculatesecondEscenary(invest_est_pes, invest_est_pro, invest_est_opt, rescate_est_pes, rescate_est_pro, rescate_est_opt, tasa_est_pes, tasa_est_pro, tasa_est_opt, newArrayFlujo):
    suma = int(invest_est_opt) + -int(invest_est_pro);
    i_est_pes = -int(invest_est_pes)
    i_est_pro = -int(invest_est_pro)
    i_est_opt = -int(invest_est_opt)
    invIni = np.around(np.random.triangular(i_est_pes, i_est_pro, i_est_opt, 1), 0) # Meter datos Estimacion pesimista - probable - optimista
    # invIni = invIni.astype(int)
    # print(invIni)
    flujo = newArrayFlujo #Meter los valores que tendra nuestro flujo
    anio = len(flujo)
    # print(anio)
    # #INDICES
    indices = np.around(np.random.uniform(0,anio-1,anio), 0) # Meter DEL - AL, y cuantos datos se desea generar
    indices = indices.astype(int) #Convierte los valores del arrreglo a enteros
    
    # #FLUJO
    # #NUEVO FLUJO SEGUN INDICES (ARREGLO NUEVO)
    flujoNuevoAux = np.take(flujo,indices)
    flujoNuevo = []
    # #VALOR DE RESCATE
    rescate = np.around(np.random.triangular(int(rescate_est_pes), int(rescate_est_pro), int(rescate_est_pro), 1), 0) #Meter datos Estimacion pesimista - probable - optimista
    rescate = rescate.astype(int).tolist()
    # print(rescate)
    # LA inflacion es alta y por eso no es rentable invertir modificando la inflación da buenos resultados
    inflacion = 1 - np.around(np.random.triangular(1-float(tasa_est_pes), 1-float(tasa_est_pro), 1-float(tasa_est_opt), 1), 3) #Meter datos Estimacion pesimista - probable - optimista

    descuento = np.around(0.15+inflacion+(0.15*inflacion), 8)

    # print(inflacion)
    # print(descuento)
    # # #SUMAR EL ULTIMO VALOR DEL FLUJO CON EL VALOR DE RESCATE Y CONCATENAR EL FLUJO RESULTANTE CON INV-INI
    suma = np.take(flujoNuevoAux,anio-1) + rescate
    flujoNuevoAux[anio-1] = suma
    for i in flujoNuevoAux:
        flujo = {'data': int(i)}  # Convert the id to a regular int
        flujoNuevo.append(flujo)
    # print(flujoNuevo)
    
    arregloFinal = np.concatenate((invIni,flujoNuevoAux),axis=0)
    # print(arregloFinal)
    # #   #REALIZAR EL CALUCULO VPN (NPV EN INGLES) [np.npv(rate,cashflow)]
    vpn =  np.npv(descuento,arregloFinal)
    # print(vpn)
    res = np.concatenate((arregloFinal, vpn, inflacion), axis = None).tolist()
    # print(res)
    return res
    # return jsonify({"result":res,"inv_ini": invIni[0],"flujo":flujoNuevo, "rescate":rescate,"vpn":vpn})

@app.route('/first-data/<id>', methods=['GET'])
def getFirstData(id):
    result = Data_Uno.query.get(id)
    return data_uno_schema.jsonify(result)
@app.route('/second-data/<idProyc>', methods=['GET'])
def getSecondData(idProyc):
    result_2 = db.session.query(Data_Dos).filter(
        Data_Dos.id_proyc == idProyc
    )
    return datas_dos_schema.jsonify(result_2)
    # return jsonify({"data":auxArrayData})
@app.route('/flujo-second-data/<idProyc>', methods=['GET'])
def getFlujoSecondData(idProyc):
    result = db.session.query(Flujo_Neto).filter(
        Flujo_Neto.id_proyc == idProyc
    )
    return flujos_netos_schema.jsonify(result)
    # return jsonify({"data":auxArrayData})
@app.route('/name-proyect/<id>', methods=['GET'])
def getNameProyect(id):
    result = Proyect.query.get(id)
    return proyc_schema.jsonify(result)

# @app.route('/tasks', methods=['GET'])
# def getTasks():
#     allTask = Task.query.all()
#     result = tasks_schema.dump(allTask)
#     return jsonify(result)

@app.route('/', methods=['GET'])
def getTask():
    return "recived"
# @app.route('/tasks/<id>', methods=['GET'])
# def getTask(id):
#     result = Task.query.get(id)
#     return task_schema.jsonify(result)


if __name__ == "__main__":
    app.run(debug=True)